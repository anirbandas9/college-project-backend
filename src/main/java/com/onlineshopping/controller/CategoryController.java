package com.onlineshopping.controller;

import java.util.List;

import com.onlineshopping.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.onlineshopping.dao.CategoryDao;
import com.onlineshopping.model.Category;

@RestController
@RequestMapping("api/category")
@CrossOrigin(origins = "http://localhost:3000")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@GetMapping("all")
	public ResponseEntity<List<Category>> getAllCategories() {
        System.out.println("request came for getting all categories");
		return categoryService.getAllCategories();
	}
	
	@PostMapping("add")
	public ResponseEntity add(@RequestBody Category category) {
		System.out.println("request came for add category");
		return categoryService.add(category);
	}
	
}

