package com.onlineshopping.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.onlineshopping.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlineshopping.dao.CartDao;
import com.onlineshopping.dao.OrderDao;
import com.onlineshopping.dao.ProductDao;
import com.onlineshopping.dao.UserDao;
import com.onlineshopping.dto.MyOrderResponse;
import com.onlineshopping.dto.OrderDataResponse;
import com.onlineshopping.dto.UpdateDeliveryStatusRequest;
import com.onlineshopping.model.Cart;
import com.onlineshopping.model.Orders;
import com.onlineshopping.model.User;
import com.onlineshopping.utility.Constants.DeliveryStatus;
import com.onlineshopping.utility.Constants.DeliveryTime;
import com.onlineshopping.utility.Constants.IsDeliveryAssigned;
import com.onlineshopping.utility.Helper;

@RestController
@RequestMapping("api/user/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@PostMapping("order")
	public ResponseEntity customerOrder(@RequestParam("userId") int userId){
		System.out.println("request came for ORDER FOR CUSTOMER ID : " + userId);
		return orderService.customerOrder(userId);
	}

	@GetMapping("myorder")
	public ResponseEntity getMyOrder(@RequestParam("userId") int userId) {
		System.out.println("request came for MY ORDER for USER ID : " + userId);
		return orderService.getMyOrder(userId);
	}

	@GetMapping("admin/allorder")
	public ResponseEntity getAllOrder() {
		System.out.println("request came for FETCH ALL ORDERS");
		return orderService.getAllOrder();
	}

	@GetMapping("admin/showorder")
	public ResponseEntity getOrdersByOrderId(@RequestParam("orderId") String orderId) {
		System.out.println("request came for FETCH ORDERS BY ORDER ID : " + orderId);
		return orderService.getOrdersByOrderId(orderId);
	}

	@PostMapping("admin/order/deliveryStatus/update")
	public ResponseEntity updateOrderDeliveryStatus(@RequestBody UpdateDeliveryStatusRequest deliveryRequest) {
		System.out.println("response came for UPDATE DELIVERY STATUS");
		System.out.println(deliveryRequest);
		return orderService.updateOrderDeliveryStatus(deliveryRequest);
	}

	@PostMapping("admin/order/assignDelivery")
	public ResponseEntity assignDeliveryPersonForOrder(@RequestBody UpdateDeliveryStatusRequest deliveryRequest) {
		System.out.println("response came for ASSIGN DELIVERY PERSON FPOR ORDERS");
		System.out.println(deliveryRequest);
		return orderService.assignDeliveryPersonForOrder(deliveryRequest);
	}

	@GetMapping("delivery/myorder")
	public ResponseEntity getMyDeliveryOrders(@RequestParam("deliveryPersonId") int deliveryPersonId) {
		System.out.println("request came for MY DELIVERY ORDER for USER ID : " + deliveryPersonId);
		return orderService.getMyDeliveryOrders(deliveryPersonId);
	}

}
