package com.onlineshopping.service;

import com.onlineshopping.dao.AddressDao;
import com.onlineshopping.dao.UserDao;
import com.onlineshopping.dto.AddUserRequest;
import com.onlineshopping.dto.UserLoginRequest;
import com.onlineshopping.model.Address;
import com.onlineshopping.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Priya
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private AddressDao addressDao;

    @Override
    public ResponseEntity<?> registerUser(AddUserRequest userRequest) {
        Address address = new Address();
        address.setCity(userRequest.getCity());
        address.setPincode(userRequest.getPincode());
        address.setStreet(userRequest.getStreet());
        Address addAddress = addressDao.save(address);
        User user = new User();
        user.setAddress(addAddress);
        user.setEmailId(userRequest.getEmailId());
        user.setFirstName(userRequest.getFirstName());
        user.setLastName(userRequest.getLastName());
        user.setPhoneNo(userRequest.getPhoneNo());
        user.setPassword(userRequest.getPassword());
        user.setRole(userRequest.getRole());
        User addUser = userDao.save(user);
        System.out.println("response sent!!!");
        return ResponseEntity.ok(addUser);
    }

    @Override
    public ResponseEntity<?> loginUser(UserLoginRequest loginRequest) {
        User user = new User();
        user = userDao.findByEmailIdAndPasswordAndRole(loginRequest.getEmailId(), loginRequest.getPassword(), loginRequest.getRole());
        System.out.println("response sent!!!");
        return ResponseEntity.ok(user);
    }

    @Override
    public ResponseEntity<?> getAllDeliveryPersons() {
        List<User> deliveryPersons = this.userDao.findByRole("Delivery");
        System.out.println("response sent!!!");
        return ResponseEntity.ok(deliveryPersons);
    }
}
