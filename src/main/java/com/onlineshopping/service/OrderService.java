package com.onlineshopping.service;

import com.onlineshopping.dto.UpdateDeliveryStatusRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Priya
 */
public interface OrderService {

    public ResponseEntity customerOrder(int userId);
    public ResponseEntity getMyOrder(int userId);
    public ResponseEntity getAllOrder();
    public ResponseEntity getOrdersByOrderId(String orderId);
    public ResponseEntity updateOrderDeliveryStatus(UpdateDeliveryStatusRequest deliveryRequest);
    public ResponseEntity assignDeliveryPersonForOrder(UpdateDeliveryStatusRequest deliveryRequest);
    public ResponseEntity getMyDeliveryOrders(int deliveryPersonId);
}
