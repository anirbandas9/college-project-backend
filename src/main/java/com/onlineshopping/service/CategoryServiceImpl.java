package com.onlineshopping.service;

import com.onlineshopping.dao.CategoryDao;
import com.onlineshopping.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Priya
 */
@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public ResponseEntity<List<Category>> getAllCategories() {
        List<Category> categories = this.categoryDao.findAll();
        ResponseEntity<List<Category>> response = new ResponseEntity<>(categories, HttpStatus.OK);
        System.out.println("response sent");
        return response;
    }

    @Override
    public ResponseEntity add(Category category) {
        Category c = categoryDao.save(category);
        if(c != null) {
            System.out.println("response sent");
            return new ResponseEntity( c ,HttpStatus.OK);
        }else {
            System.out.println("response sent");
            return new ResponseEntity("Failed to add category!",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
