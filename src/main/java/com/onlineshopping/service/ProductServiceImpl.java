package com.onlineshopping.service;

import com.onlineshopping.dao.CategoryDao;
import com.onlineshopping.dao.UserDao;
import com.onlineshopping.dto.ProductAddRequest;
import com.onlineshopping.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import com.onlineshopping.dao.ProductDao;
import com.onlineshopping.model.Product;
import com.onlineshopping.utility.StorageService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired 
	private ProductDao productDao;
	
	@Autowired
	private StorageService storageService;

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private UserDao userDao;


	public void addProductImage(Product product, MultipartFile productImmage) {
		String productImageName = storageService.store(productImmage);
		product.setImageName(productImageName);
		this.productDao.save(product);
	}

	@Override
	public ResponseEntity<?> addProduct(ProductAddRequest productDto) {
		Product product=ProductAddRequest.toEntity(productDto);

		Optional<Category> optional = categoryDao.findById(productDto.getCategoryId());
		Category category = null;
		if(optional.isPresent()) {
			category = optional.get();
		}

		product.setCategory(category);
		addProductImage(product, productDto.getImage());

		System.out.println("response sent!!!");
		return ResponseEntity.ok(product);

	}

	@Override
	public ResponseEntity<?> getAllProducts() {
		List<Product> products = new ArrayList<Product>();

		products = productDao.findAll();

		System.out.println("response sent!!!");

		return ResponseEntity.ok(products);

	}

	@Override
	public ResponseEntity<?> getProductById(int productId) {
		Product product = new Product();

		Optional<Product> optional = productDao.findById(productId);

		if(optional.isPresent()) {
			product = optional.get();
		}
		System.out.println("response sent!!!");

		return ResponseEntity.ok(product);
	}

	@Override
	public ResponseEntity<?> getProductsByCategories(int categoryId) {
		List<Product> products = new ArrayList<Product>();

		products = productDao.findByCategoryId(categoryId);

		System.out.println("response sent!!!");

		return ResponseEntity.ok(products);
	}

	@Override
	public void fetchProductImage(String productImageName, HttpServletResponse resp) {
		System.out.println("request came for fetching product pic");
		System.out.println("Loading file: " + productImageName);
		Resource resource = storageService.load(productImageName);
		if(resource != null) {
			try(InputStream in = resource.getInputStream()) {
				ServletOutputStream out = resp.getOutputStream();
				FileCopyUtils.copy(in, out);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("response sent!");
	}
}
