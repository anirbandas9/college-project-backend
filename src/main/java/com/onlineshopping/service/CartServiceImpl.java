package com.onlineshopping.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlineshopping.dao.CartDao;
import com.onlineshopping.dao.ProductDao;
import com.onlineshopping.dao.UserDao;
import com.onlineshopping.dto.AddToCartRequest;
import com.onlineshopping.dto.CartDataResponse;
import com.onlineshopping.dto.CartResponse;
import com.onlineshopping.model.Cart;
import com.onlineshopping.model.Product;
import com.onlineshopping.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Priya
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartDao cartDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductDao productDao;

    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public ResponseEntity add(AddToCartRequest addToCartRequest) {
        Optional<User> optionalUser = userDao.findById(addToCartRequest.getUserId());
        User user = null;
        if(optionalUser.isPresent()) {
            user = optionalUser.get();
        }

        Optional<Product> optionalProduct = productDao.findById(addToCartRequest.getProductId());
        Product product = null;
        if(optionalProduct.isPresent()) {
            product = optionalProduct.get();
        }

        Cart cart = new Cart();
        cart.setProduct(product);
        cart.setQuantity(addToCartRequest.getQuantity());
        cart.setUser(user);

        cartDao.save(cart);

        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity getMyCart(int userId) {
        List<CartDataResponse> cartDatas = new ArrayList<>();
        List<Cart> userCarts = cartDao.findByUser_id(userId);

        double totalCartPrice = 0;

        for (Cart cart : userCarts) {
            CartDataResponse cartData = new CartDataResponse();
            cartData.setCartId(cart.getId());
            cartData.setProductDescription(cart.getProduct().getDescription());
            cartData.setProductName(cart.getProduct().getTitle());
            cartData.setProductImage(cart.getProduct().getImageName());
            cartData.setQuantity(cart.getQuantity());
            cartData.setProductId(cart.getProduct().getId());

            cartDatas.add(cartData);

            double productPrice = Double.parseDouble(cart.getProduct().getPrice().toString());

            totalCartPrice =  totalCartPrice + (cart.getQuantity() * productPrice);
        }

        CartResponse cartResponse = new CartResponse();
        cartResponse.setTotalCartPrice(String.valueOf(totalCartPrice));
        cartResponse.setCartData(cartDatas);

        String json = null;
        try {
            json = objectMapper.writeValueAsString(cartResponse);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        System.out.println(json);
        return new ResponseEntity(cartResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity removeCartItem(int cartId) {
        Optional<Cart> optionalCart = this.cartDao.findById(cartId);
        Cart cart = new Cart();

        if(optionalCart.isPresent()) {
            cart = optionalCart.get();
        }
        this.cartDao.delete(cart);
        return new ResponseEntity("SUCCESS", HttpStatus.OK);
    }
}
