package com.onlineshopping.service;

import com.onlineshopping.dto.ProductAddRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.onlineshopping.model.Product;

import javax.servlet.http.HttpServletResponse;

public interface ProductService {
	ResponseEntity<?> addProduct(ProductAddRequest productDto);
	ResponseEntity<?> getAllProducts();
	ResponseEntity<?> getProductById(int productId);
	ResponseEntity<?> getProductsByCategories(int categoryId);
	void fetchProductImage(String productImageName, HttpServletResponse resp);
}
