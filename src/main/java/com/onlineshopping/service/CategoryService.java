package com.onlineshopping.service;

import com.onlineshopping.model.Category;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author Priya
 */
public interface CategoryService {

    public ResponseEntity<List<Category>> getAllCategories();
    public ResponseEntity add(Category category);


}
