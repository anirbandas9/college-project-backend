package com.onlineshopping.service;

import com.onlineshopping.dto.AddToCartRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Priya
 */
public interface CartService {

    public ResponseEntity add(AddToCartRequest addToCartRequest) ;
    public ResponseEntity getMyCart(int userId) ;
    public ResponseEntity removeCartItem(int cartId) ;

}
