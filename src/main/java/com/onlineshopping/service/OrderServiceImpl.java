package com.onlineshopping.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlineshopping.dao.CartDao;
import com.onlineshopping.dao.OrderDao;
import com.onlineshopping.dao.ProductDao;
import com.onlineshopping.dao.UserDao;
import com.onlineshopping.dto.MyOrderResponse;
import com.onlineshopping.dto.OrderDataResponse;
import com.onlineshopping.dto.UpdateDeliveryStatusRequest;
import com.onlineshopping.model.Cart;
import com.onlineshopping.model.Orders;
import com.onlineshopping.model.User;
import com.onlineshopping.utility.Constants;
import com.onlineshopping.utility.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Order;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Priya
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private CartDao cartDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductDao productDao;

    private ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public ResponseEntity customerOrder(int userId) {
        String orderId = Helper.getAlphaNumericOrderId();

        List<Cart> userCarts = cartDao.findByUser_id(userId);

        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        String formatDateTime = currentDateTime.format(formatter);

        for (Cart cart : userCarts) {

            Orders order = new Orders();
            order.setOrderId(orderId);
            order.setUser(cart.getUser());
            order.setProduct(cart.getProduct());
            order.setQuantity(cart.getQuantity());
            order.setOrderDate(formatDateTime);
            order.setDeliveryDate(Constants.DeliveryStatus.PENDING.value());
            order.setDeliveryStatus(Constants.DeliveryStatus.PENDING.value());
            order.setDeliveryTime(Constants.DeliveryTime.DEFAULT.value());
            order.setDeliveryAssigned(Constants.IsDeliveryAssigned.NO.value());

            orderDao.save(order);
            cartDao.delete(cart);
        }

        System.out.println("response sent!!!");

        return new ResponseEntity("ORDER SUCCESS", HttpStatus.OK);
    }

    @Override
    public ResponseEntity getMyOrder(int userId) {
        List<Orders> userOrder = orderDao.findByUser_id(userId);

        OrderDataResponse orderResponse = new OrderDataResponse();

        List<MyOrderResponse> orderDatas = new ArrayList<>();

        for (Orders order : userOrder) {
            MyOrderResponse orderData = new MyOrderResponse();
            orderData.setOrderId(order.getOrderId());
            orderData.setProductDescription(order.getProduct().getDescription());
            orderData.setProductName(order.getProduct().getTitle());
            orderData.setProductImage(order.getProduct().getImageName());
            orderData.setQuantity(order.getQuantity());
            orderData.setOrderDate(order.getOrderDate());
            orderData.setProductId(order.getProduct().getId());
            orderData.setDeliveryDate(order.getDeliveryDate() + " " + order.getDeliveryTime());
            orderData.setDeliveryStatus(order.getDeliveryStatus());
            orderData.setTotalPrice(
                    String.valueOf(order.getQuantity() * Double.parseDouble(order.getProduct().getPrice().toString())));
            if (order.getDeliveryPersonId() == 0) {
                orderData.setDeliveryPersonContact(Constants.DeliveryStatus.PENDING.value());
                orderData.setDeliveryPersonName(Constants.DeliveryStatus.PENDING.value());
            } else {

                User deliveryPerson = null;

                Optional<User> optionalDeliveryPerson = this.userDao.findById(order.getDeliveryPersonId());

                deliveryPerson = optionalDeliveryPerson.get();

                orderData.setDeliveryPersonContact(deliveryPerson.getPhoneNo());
                orderData.setDeliveryPersonName(deliveryPerson.getFirstName());
            }
            orderDatas.add(orderData);
        }

        String json = null;
        try {
            json = objectMapper.writeValueAsString(orderDatas);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        System.out.println(json);

        return new ResponseEntity(orderDatas, HttpStatus.OK);
    }

    @Override
    public ResponseEntity getAllOrder() {
        List<Orders> userOrder = orderDao.findAll();

        OrderDataResponse orderResponse = new OrderDataResponse();

        List<MyOrderResponse> orderDatas = new ArrayList<>();

        for (Orders order : userOrder) {
            MyOrderResponse orderData = new MyOrderResponse();
            orderData.setOrderId(order.getOrderId());
            orderData.setProductDescription(order.getProduct().getDescription());
            orderData.setProductName(order.getProduct().getTitle());
            orderData.setProductImage(order.getProduct().getImageName());
            orderData.setQuantity(order.getQuantity());
            orderData.setOrderDate(order.getOrderDate());
            orderData.setProductId(order.getProduct().getId());
            orderData.setDeliveryDate(order.getDeliveryDate() + " " + order.getDeliveryTime());
            orderData.setDeliveryStatus(order.getDeliveryStatus());
            orderData.setTotalPrice(
                    String.valueOf(order.getQuantity() * Double.parseDouble(order.getProduct().getPrice().toString())));
            orderData.setUserId(order.getUser().getId());
            orderData.setUserName(order.getUser().getFirstName() + " " + order.getUser().getLastName());
            orderData.setUserPhone(order.getUser().getPhoneNo());
            orderData.setAddress(order.getUser().getAddress());
            if (order.getDeliveryPersonId() == 0) {
                orderData.setDeliveryPersonContact(Constants.DeliveryStatus.PENDING.value());
                orderData.setDeliveryPersonName(Constants.DeliveryStatus.PENDING.value());
            } else {
                User deliveryPerson = null;

                Optional<User> optionalDeliveryPerson = this.userDao.findById(order.getDeliveryPersonId());

                deliveryPerson = optionalDeliveryPerson.get();

                orderData.setDeliveryPersonContact(deliveryPerson.getPhoneNo());
                orderData.setDeliveryPersonName(deliveryPerson.getFirstName());
            }
            orderDatas.add(orderData);

        }

        String json = null;
        try {
            json = objectMapper.writeValueAsString(orderDatas);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        System.out.println(json);

        System.out.println("response sent !!!");

        return new ResponseEntity(orderDatas, HttpStatus.OK);
    }

    @Override
    public ResponseEntity getOrdersByOrderId(String orderId) {
        List<Orders> userOrder = orderDao.findByOrderId(orderId);

        List<MyOrderResponse> orderDatas = new ArrayList<>();

        for (Orders order : userOrder) {
            MyOrderResponse orderData = new MyOrderResponse();
            orderData.setOrderId(order.getOrderId());
            orderData.setProductDescription(order.getProduct().getDescription());
            orderData.setProductName(order.getProduct().getTitle());
            orderData.setProductImage(order.getProduct().getImageName());
            orderData.setQuantity(order.getQuantity());
            orderData.setOrderDate(order.getOrderDate());
            orderData.setProductId(order.getProduct().getId());
            orderData.setDeliveryDate(order.getDeliveryDate() + " " + order.getDeliveryTime());
            orderData.setDeliveryStatus(order.getDeliveryStatus());
            orderData.setTotalPrice(
                    String.valueOf(order.getQuantity() * Double.parseDouble(order.getProduct().getPrice().toString())));
            orderData.setUserId(order.getUser().getId());
            orderData.setUserName(order.getUser().getFirstName() + " " + order.getUser().getLastName());
            orderData.setUserPhone(order.getUser().getPhoneNo());
            orderData.setAddress(order.getUser().getAddress());
            if (order.getDeliveryPersonId() == 0) {
                orderData.setDeliveryPersonContact(Constants.DeliveryStatus.PENDING.value());
                orderData.setDeliveryPersonName(Constants.DeliveryStatus.PENDING.value());
            } else {
                User deliveryPerson = null;

                Optional<User> optionalDeliveryPerson = this.userDao.findById(order.getDeliveryPersonId());

                deliveryPerson = optionalDeliveryPerson.get();

                orderData.setDeliveryPersonContact(deliveryPerson.getPhoneNo());
                orderData.setDeliveryPersonName(deliveryPerson.getFirstName());
            }
            orderDatas.add(orderData);

        }

        String json = null;
        try {
            json = objectMapper.writeValueAsString(orderDatas);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        System.out.println(json);
        System.out.println("response sent !!!");
        return new ResponseEntity(orderDatas, HttpStatus.OK);
    }

    @Override
    public ResponseEntity updateOrderDeliveryStatus(UpdateDeliveryStatusRequest deliveryRequest) {
        List<Orders> orders = orderDao.findByOrderId(deliveryRequest.getOrderId());

        for (Orders order : orders) {
            order.setDeliveryDate(deliveryRequest.getDeliveryDate());
            order.setDeliveryStatus(deliveryRequest.getDeliveryStatus());
            order.setDeliveryTime(deliveryRequest.getDeliveryTime());
            orderDao.save(order);
        }

        List<Orders> userOrder = orderDao.findByOrderId(deliveryRequest.getOrderId());

        List<MyOrderResponse> orderDatas = new ArrayList<>();

        for (Orders order : userOrder) {
            MyOrderResponse orderData = new MyOrderResponse();
            orderData.setOrderId(order.getOrderId());
            orderData.setProductDescription(order.getProduct().getDescription());
            orderData.setProductName(order.getProduct().getTitle());
            orderData.setProductImage(order.getProduct().getImageName());
            orderData.setQuantity(order.getQuantity());
            orderData.setOrderDate(order.getOrderDate());
            orderData.setProductId(order.getProduct().getId());
            orderData.setDeliveryDate(order.getDeliveryDate() + " " + order.getDeliveryTime());
            orderData.setDeliveryStatus(order.getDeliveryStatus());
            orderData.setTotalPrice(
                    String.valueOf(order.getQuantity() * Double.parseDouble(order.getProduct().getPrice().toString())));
            orderData.setUserId(order.getUser().getId());
            orderData.setUserName(order.getUser().getFirstName() + " " + order.getUser().getLastName());
            orderData.setUserPhone(order.getUser().getPhoneNo());
            orderData.setAddress(order.getUser().getAddress());
            if (order.getDeliveryPersonId() == 0) {
                orderData.setDeliveryPersonContact(Constants.DeliveryStatus.PENDING.value());
                orderData.setDeliveryPersonName(Constants.DeliveryStatus.PENDING.value());
            } else {
                User deliveryPerson = null;

                Optional<User> optionalDeliveryPerson = this.userDao.findById(order.getDeliveryPersonId());

                deliveryPerson = optionalDeliveryPerson.get();

                orderData.setDeliveryPersonContact(deliveryPerson.getPhoneNo());
                orderData.setDeliveryPersonName(deliveryPerson.getFirstName());
            }
            orderDatas.add(orderData);

        }

        String orderJson = null;
        try {
            orderJson = objectMapper.writeValueAsString(orderDatas);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        System.out.println(orderJson);

        System.out.println("response sent !!!");

        return new ResponseEntity(orderDatas, HttpStatus.OK);
    }

    @Override
    public ResponseEntity assignDeliveryPersonForOrder(UpdateDeliveryStatusRequest deliveryRequest) {
        List<Orders> orders = orderDao.findByOrderId(deliveryRequest.getOrderId());

        User deliveryPerson = null;

        Optional<User> optionalDeliveryPerson = this.userDao.findById(deliveryRequest.getDeliveryId());

        if (optionalDeliveryPerson.isPresent()) {
            deliveryPerson = optionalDeliveryPerson.get();
        }

        for (Orders order : orders) {
            order.setDeliveryAssigned(Constants.IsDeliveryAssigned.YES.value());
            order.setDeliveryPersonId(deliveryRequest.getDeliveryId());
            orderDao.save(order);
        }

        List<Orders> userOrder = orderDao.findByOrderId(deliveryRequest.getOrderId());

        List<MyOrderResponse> orderDatas = new ArrayList<>();

        for (Orders order : userOrder) {
            MyOrderResponse orderData = new MyOrderResponse();
            orderData.setOrderId(order.getOrderId());
            orderData.setProductDescription(order.getProduct().getDescription());
            orderData.setProductName(order.getProduct().getTitle());
            orderData.setProductImage(order.getProduct().getImageName());
            orderData.setQuantity(order.getQuantity());
            orderData.setOrderDate(order.getOrderDate());
            orderData.setProductId(order.getProduct().getId());
            orderData.setDeliveryDate(order.getDeliveryDate() + " " + order.getDeliveryTime());
            orderData.setDeliveryStatus(order.getDeliveryStatus());
            orderData.setTotalPrice(
                    String.valueOf(order.getQuantity() * Double.parseDouble(order.getProduct().getPrice().toString())));
            orderData.setUserId(order.getUser().getId());
            orderData.setUserName(order.getUser().getFirstName() + " " + order.getUser().getLastName());
            orderData.setUserPhone(order.getUser().getPhoneNo());
            orderData.setAddress(order.getUser().getAddress());

            if (order.getDeliveryPersonId() == 0) {
                orderData.setDeliveryPersonContact(Constants.DeliveryStatus.PENDING.value());
                orderData.setDeliveryPersonName(Constants.DeliveryStatus.PENDING.value());
            } else {
                User dPerson = null;

                Optional<User> optionalPerson = this.userDao.findById(order.getDeliveryPersonId());

                dPerson = optionalPerson.get();

                orderData.setDeliveryPersonContact(dPerson.getPhoneNo());
                orderData.setDeliveryPersonName(dPerson.getFirstName());
            }

            orderDatas.add(orderData);

        }

        String orderJson = null;
        try {
            orderJson = objectMapper.writeValueAsString(orderDatas);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        System.out.println(orderJson);

        System.out.println("response sent !!!");

        return new ResponseEntity(orderDatas, HttpStatus.OK);
    }

    @Override
    public ResponseEntity getMyDeliveryOrders(int deliveryPersonId) {
        User person = null;

        Optional<User> oD = this.userDao.findById(deliveryPersonId);

        if (oD.isPresent()) {
            person = oD.get();
        }

        List<Orders> userOrder = orderDao.findByDeliveryPersonId(deliveryPersonId);

        List<MyOrderResponse> orderDatas = new ArrayList<>();

        for (Orders order : userOrder) {
            MyOrderResponse orderData = new MyOrderResponse();
            orderData.setOrderId(order.getOrderId());
            orderData.setProductDescription(order.getProduct().getDescription());
            orderData.setProductName(order.getProduct().getTitle());
            orderData.setProductImage(order.getProduct().getImageName());
            orderData.setQuantity(order.getQuantity());
            orderData.setOrderDate(order.getOrderDate());
            orderData.setProductId(order.getProduct().getId());
            orderData.setDeliveryDate(order.getDeliveryDate() + " " + order.getDeliveryTime());
            orderData.setDeliveryStatus(order.getDeliveryStatus());
            orderData.setTotalPrice(
                    String.valueOf(order.getQuantity() * Double.parseDouble(order.getProduct().getPrice().toString())));
            orderData.setUserId(order.getUser().getId());
            orderData.setUserName(order.getUser().getFirstName() + " " + order.getUser().getLastName());
            orderData.setUserPhone(order.getUser().getPhoneNo());
            orderData.setAddress(order.getUser().getAddress());

            if (order.getDeliveryPersonId() == 0) {
                orderData.setDeliveryPersonContact(Constants.DeliveryStatus.PENDING.value());
                orderData.setDeliveryPersonName(Constants.DeliveryStatus.PENDING.value());
            } else {
                orderData.setDeliveryPersonContact(person.getPhoneNo());
                orderData.setDeliveryPersonName(person.getFirstName());
            }

            orderDatas.add(orderData);

        }

        String json = null;
        try {
            json = objectMapper.writeValueAsString(orderDatas);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        System.out.println(json);

        return new ResponseEntity(orderDatas, HttpStatus.OK);

    }
}
