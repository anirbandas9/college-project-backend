package com.onlineshopping.service;

import com.onlineshopping.dto.AddUserRequest;
import com.onlineshopping.dto.UserLoginRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Priya
 */
public interface UserService {
    ResponseEntity<?> registerUser(AddUserRequest userRequest);
    ResponseEntity<?> loginUser(UserLoginRequest loginRequest);
    ResponseEntity<?> getAllDeliveryPersons();
}
